const LotteryColorStyle = {
    "CF201": [
        '#f1a73c',
        '#ff738e',
        '#5ccafc',
        '#f17754',
        '#ff4e70',
        '#42c0b9',
        '#48d7ca',
        '#7a86ce',
    ],
    "STA01": [
        '#f1a73c',
        '#ff738e',
        '#5ccafc',
        '#f17754',
        '#ff4e70',
        '#42c0b9',
        '#48d7ca',
        '#7a86ce',
    ],
    "VT999": [
        'linear-gradient(to right, #f1f140, #feb927)',
        'linear-gradient(to right, #ef96cb, #dd66b8)',
        'linear-gradient(to right, #53bcfa, #4a78d9)',
        'linear-gradient(to right, #ffb36f, #dc7a5a)',
        'linear-gradient(to right, #f88581, #da5f5f)',
        'linear-gradient(to right, #67d5b0, #70b85f)',
        'linear-gradient(to right, #6ce8e8, #24a1a8)',
        'linear-gradient(to right, #cc8dd8, #7762b2)',
    ],
}

export default LotteryColorStyle;