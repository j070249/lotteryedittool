const ThirdPartyGameStyle = {
    "CF201": [
        'linear-gradient(to bottom, #1ccde2, #20e4c8)',
        'linear-gradient(to bottom, #ff956e, #ff5f8f)',
        'linear-gradient(to bottom, #bd41d0, #6c56d2)',
        'linear-gradient(to bottom, #1ccde2, #20e4c8)',
        'linear-gradient(to bottom, #ff956e, #ff5f8f)',
        'linear-gradient(to bottom, #bd41d0, #6c56d2)',
        'linear-gradient(to bottom, #1ccde2, #20e4c8)',
        'linear-gradient(to bottom, #ff956e, #ff5f8f)',
    ],
    "STA01": [
        'linear-gradient(to bottom, #1ccde2, #20e4c8)',
        'linear-gradient(to bottom, #ff956e, #ff5f8f)',
        'linear-gradient(to bottom, #bd41d0, #6c56d2)',
        'linear-gradient(to bottom, #1ccde2, #20e4c8)',
        'linear-gradient(to bottom, #ff956e, #ff5f8f)',
        'linear-gradient(to bottom, #bd41d0, #6c56d2)',
        'linear-gradient(to bottom, #1ccde2, #20e4c8)',
        'linear-gradient(to bottom, #ff956e, #ff5f8f)',
    ],
    "VT999": [
        'linear-gradient(to right, #778BDD, #5FDCD9)',
        'linear-gradient(to right, #E78E81, #FFB36F)',
        'linear-gradient(to right, #82CA45, #8CEC7A)',
        'linear-gradient(to right, #7762B2, #CC8DD8)',
        'linear-gradient(to right, #DD66B8, #EF96CB)',
        'linear-gradient(to right, #FEB927, #F3E93D)',
        'linear-gradient(to right, #24A1A8, #6CE8E8)',
        'linear-gradient(to right, #DA5F5F, #F88581)',
    ]
}

export default ThirdPartyGameStyle;