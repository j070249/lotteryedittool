import * as HttpService from './httpService';

export const getLotteryFile = (url, agent) => HttpService.Rserver_get(`${url}/static-files/${agent}/AppConfig/lottery.json`, "");

export const getThirdPartyGameFile = (url, agent) => HttpService.Rserver_get(`${url}/static-files/${agent}/AppConfig/third_party_game.json`, "");

export const getMineFile = (url, agent) => HttpService.Rserver_get(`${url}/static-files/${agent}/AppConfig/mine_centers.json`, "");

export const postLotteryFile = (agent, file) => HttpService.Rserver_post(``, file);

export const postThirdPartyGameFile = (agent, file) => HttpService.Rserver_post(``, file);

export const getLotteriesFile = (url, path) => HttpService.Rserver_get(`${url}${path}`, "");
//export const getLotteriesFile = (url,agent) => HttpService.Rserver_get(`${url}/static-files/${agent}/Lottery/android/zh-CN/lotteries-1629688535.json`, "");