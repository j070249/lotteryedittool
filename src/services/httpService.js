import axios from 'axios';

export const post = async (url, data, config) =>
  axios({
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    },
    url: `http://localhost:8888/api/${url}`,
    data: data,
    ...config,
  }).then((x) => x);

export const get = async (url, params, config) =>
  axios({
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    },
    url: `http://localhost:8888/api/${url}`,
    params: params,
    ...config,
  }).then((x) => x);

export const timeoutpost = async (url, data, config) =>
  axios({
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    },
    timeout: 5000,
    url: `${url}`,
    data: data,
    ...config,
  }).then((x) => x);

export const Rserver_post = async (url, data, config) =>
  axios({
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
      'Cache-Control': 'no-cache'
    },
    url: `${url}`,
    data: data,
    ...config,
  }).then((x) => x);

export const Rserver_get = async (url, params, config) =>
  axios({
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    },
    url: `${url}`,
    params: params,
    ...config,
  }).then((x) => x)
    .catch((e) => {
      console.log(e);
      return 'error'
    });
