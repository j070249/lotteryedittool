import { Image } from 'antd';

const ImageSrc = (img = "", background = '#000000', isPreview = false, width = 100) => {
    var imgSrc = 'error';
    
    try {
        if (img.includes('http'))
            imgSrc = img
        else
            imgSrc = require(`../assets/images/${img}.webp`).default;

    } catch (error) {
        try {
            if (img.includes('http'))
                imgSrc = img
            else
                imgSrc = require(`../assets/images/${img}.png`).default;
        } catch (error) {
        }
    }

    return (
        <Image
            width={width}
            src={imgSrc}
            alt={img}
            preview={isPreview}
            style={{ background: imgSrc === 'error' ? '' : background }}
        />
    )
}

export default ImageSrc;