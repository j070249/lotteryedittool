import React, { useState, useEffect, useContext } from 'react';
import { Layout, Menu, Row, Col, Radio } from 'antd';
import {
  FileOutlined,
} from '@ant-design/icons';
import LotteryJsonApply from './firstPage/lotteryJsonApply'
import ThirdPartyGameJsonApply from './firstPage/thirdPartyGameJsonApply'
import MinePageJsonApply from './firstPage/minePageJsonApply'
import logo from '../logo.svg';
import * as fileApi from '../services/fileApi'
import * as hostApi from '../services/hostApi'
import { StoreContext } from '../provider/storeProvider'

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const FirstPage = () => {

  const [collapsed, setCollapsed] = useState(false);
  const [itemKey, setItemKey] = useState("1");
  const [lotteryAllData, setLotteryAllData] = useState([])
  const [thirdPartyAllData, setThirdPartyAllData] = useState([])
  const [mineAllData, setMineAllData] = useState([])
  const { urlState, agentState, platformState } = useContext(StoreContext);
  const [url, setUrl] = urlState;
  const [agent, setAgent] = agentState
  const [platform, setPlatform] = platformState

  useEffect(() => {
    setPlatform('android')
    checkAPI(agent, itemKey)
  }, [setUrl])

  const onCollapse = (collapsed) => {
    setCollapsed(collapsed)
  };

  const onClick = async (e) => {
    setItemKey(e.keyPath[0])
    checkAPI(e.keyPath[1], e.keyPath[0])
  }

  const fetchAllLotteryData = async (agent, itemIndex, url, path) => {
    setUrl(url)
    if (path === '') return
    if (itemIndex % 3 === 1) {
      const response = await fileApi.getLotteriesFile(url, path)
      setLotteryAllData(response?.data)
    } else if (itemIndex % 3 === 2) {
      const response = await fileApi.getLotteriesFile(url, `/static-files/${agent}/GameProvider/${platform}/gameProvider.json`)
      setThirdPartyAllData(response?.data)
    } else {
      const response = await fileApi.getLotteriesFile(url, `/static-files/${agent}/Mine/${platform}/mines.json`)
      setMineAllData(response?.data)
    }
  }

  const checkAPI = async (agent, itemIndex) => {
    if (typeof agent == 'undefined') return

    // console.log('platform', platform)
    const res = await hostApi.getHostApi(agent, platform)
    if (res === 'error') return
    // console.log('data', res)
    const url = res?.data?.data?.endpoints?.storage[0]
    var path = ''
    if (itemIndex % 3 === 1) {
      path = res?.data?.data?.lotteries?.find((lottery) => lottery.codeName === 'lotteries')?.path
    } else if (itemIndex % 3 === 2) {
      path = res?.data?.data?.gameProviderType?.path
    } else {
      path = '/'
    }
    setAgent(agent)
    fetchAllLotteryData(agent, itemIndex, url, path)
  }

  const [value, setValue] = React.useState(1);

  const onChange = e => {
    setValue(e.target.value);

    if (e.target.value === 1) {
      setPlatform("android")
    }
    if (e.target.value === 2) {
      setPlatform("ios")
    }
    if (e.target.value === 3) {
      setPlatform("h5")
    }
    if (e.target.value === 4) {
      setPlatform("web")
    }
  };

  const pages = [
    <MinePageJsonApply data={mineAllData} />,
    <LotteryJsonApply data={lotteryAllData} />,
    <ThirdPartyGameJsonApply data={thirdPartyAllData} />
  ]

  const titles = [
    '個人頁',
    '彩票',
    '第三方遊戲'
  ]

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
        <div className="logo" />
        <img src={logo} className="logo" alt="logo" />

        <Radio.Group style={{ width: '100%' }} onChange={onChange} value={value}>
          <Row>
            <Col span={12}>
              <Radio value={1}>{<div style={{ display: 'flex', justifyContent: 'center', color: '#FFFFFF' }}>android</div>}</Radio>
            </Col>
            <Col span={12}>
              <Radio value={2}>{<div style={{ display: 'flex', justifyContent: 'center', color: '#FFFFFF' }}>ios</div>}</Radio>
            </Col>
            <Col span={12}>
              <Radio value={3}>{<div style={{ display: 'flex', justifyContent: 'center', color: '#FFFFFF' }}>h5</div>}</Radio>
            </Col>
            <Col span={12}>
              <Radio value={4}>{<div style={{ display: 'flex', justifyContent: 'center', color: '#FFFFFF' }}>web</div>}</Radio>
            </Col>
          </Row>
        </Radio.Group>

        <Menu
          theme="dark"
          defaultOpenKeys={['CF201']}
          defaultSelectedKeys={['CF201']}
          mode="inline"
          onClick={onClick}
          selectedKeys={[itemKey]}>

          <SubMenu key="CF201" icon={<FileOutlined />} title="CF201">
            <Menu.Item key="1">彩票</Menu.Item>
            <Menu.Item key="2">第三方遊戲</Menu.Item>
            <Menu.Item key="3">個人頁</Menu.Item>
          </SubMenu>
          <SubMenu key="STA01" icon={<FileOutlined />} title="STA01">
            <Menu.Item key="4">彩票</Menu.Item>
            <Menu.Item key="5">第三方遊戲</Menu.Item>
            <Menu.Item key="6">個人頁</Menu.Item>
          </SubMenu>
          <SubMenu key="VT999" icon={<FileOutlined />} title="VT999">
            <Menu.Item key="7">彩票</Menu.Item>
            <Menu.Item key="8">第三方遊戲</Menu.Item>
            <Menu.Item key="9">個人頁</Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>{<div style={{ display: 'flex', justifyContent: 'center', color: '#FFFFFF', fontSize: 20, fontWeight: 'bold' }}>{`${agent}${titles[itemKey % 3]}`}</div>}</Header>
        <Content style={{ margin: '0 16px' }}>
          {
            (typeof agent !== 'undefined') ? pages[itemKey % 3] : <div/>
          }
        </Content>
        <Footer style={{ textAlign: 'center' }}>Test app ©2021 Created by Joe</Footer>
      </Layout>
    </Layout>
  );
}

export default FirstPage
