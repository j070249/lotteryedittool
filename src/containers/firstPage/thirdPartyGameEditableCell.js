import { Input, Select, Form } from 'antd';
import ImageSrc from '../../components/imageSrc'
import ThirdPartyGameStyle from '../../assets/styles/thirdPartyGameStyle'

const { Option } = Select;

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  categoryCode,
  agent,
  ...restProps
}) => {
  var inputNode = <Input />

  const staGameIconLotterys = []

  const vtGameIconLotterys = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery01.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery02.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery03.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery04.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery05.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery06.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery07.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery08.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_lottery09.webp'
  ]

  const staGameIconCasinos = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_ag.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_bg.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_dg.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_ob_casino.webp'
  ]

  const vtGameIconCasinos = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_livedealer01.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_livedealer02.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_livedealer03.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_livedealer04.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_livedealer05.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_livedealer06.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_livedealer07.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_livedealer08.webp'
  ]

  const staGameIconSlots = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_mg.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/big_gameicon_ag.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/big_gameicon_bg.webp'
  ]

  const vtGameIconSlots = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot01.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot02.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot03.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot04.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot05.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot06.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot07.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot08.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_slot09.webp'
  ]

  const staGameIconSports = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_ftg.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_saba.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_im_rect.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_cmd.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_ob_sport.webp'
  ]

  const vtGameIconSports = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_sport01.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_sport02.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_sport03.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_sport04.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_sport05.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_sport06.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_sport07.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_sport08.webp'
  ]

  const staGameIconPokers = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/big_gameicon_ky.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/big_gameicon_bl.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_ky.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/big_gameicon_leg.webp'
  ]

  const vtGameIconPokers = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_poker01.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_poker02.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_poker03.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_poker04.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_poker05.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_poker06.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_poker07.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_poker08.webp'
  ]

  const staGameIconFishings = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_ag.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_bg.webp'
  ]

  const vtGameIconFishings = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_fishing01.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_fishing02.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_fishing03.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_fishing04.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_fishing05.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_fishing06.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_fishing07.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_fishing08.webp'
  ]

  const staGameIconEsports = [
  ]

  const vtGameIconEsports = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_esport01.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_esport02.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_esport03.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_esport04.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_esport05.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_esport06.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_esport07.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_esport08.webp'
  ]

  const staGameIconCockfightings = [
  ]

  const vtGameIconCockfightings = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/game_icon_cockfighting01.webp'
  ]

  if (title === 'isLobbyEntry' || title === 'isSupportDesktopWeb' || title === 'Enabled') {
    inputNode = <Select>
      <Option value={false}>false</Option>
      <Option value={true}>true</Option>
    </Select>
  } else if (title === 'gameProviderBgResIndex') {
    var indents = [];
    for (var i = 0; i < 8; i++) {
      indents.push(<Option value={i}>{<div style={{ width: 100, height: 100, background: ThirdPartyGameStyle[agent][i] }}>{i}</div>}</Option>);
    }
    inputNode = <Select>
      {indents}
    </Select>
  } else if (title === 'gameProviderIconName') {
    switch (categoryCode) {
      case '0': //首頁
        inputNode = <Select size='large' style={{ overflow: 'auto' }}>
        </Select>
        break
      case '1': //彩票
        switch (agent) {
          case 'CF201':
          case 'STA01':
            break
          case 'VT999':
            inputNode = <Select size='large' style={{ overflow: 'auto' }}>
              {
                vtGameIconLotterys?.map((staGameIconCasino, index) => {
                  return (<Option value={staGameIconCasino}>
                    {ImageSrc(staGameIconCasino, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '2': //視訊
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                staGameIconCasinos?.map((vtGameIconLottery, index) => {
                  return (<Option value={vtGameIconLottery}>
                    {ImageSrc(vtGameIconLottery, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            inputNode = <Select size='large' style={{ overflow: 'auto' }}>
              {
                vtGameIconCasinos?.map((vtGameIconCasino, index) => {
                  return (<Option value={vtGameIconCasino}>
                    {ImageSrc(vtGameIconCasino, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '3': //電子
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                staGameIconSlots?.map((staGameIconSlot, index) => {
                  return (<Option value={staGameIconSlot}>
                    {ImageSrc(staGameIconSlot, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            inputNode = <Select size='large' style={{ overflow: 'auto' }}>
              {
                vtGameIconSlots?.map((vtGameIconSlot, index) => {
                  return (<Option value={vtGameIconSlot}>
                    {ImageSrc(vtGameIconSlot, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '4': //體育
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                staGameIconSports?.map((staGameIconSport, index) => {
                  return (<Option value={staGameIconSport}>
                    {ImageSrc(staGameIconSport, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            inputNode = <Select size='large' style={{ overflow: 'auto' }}>
              {
                vtGameIconSports?.map((vtGameIconSport, index) => {
                  return (<Option value={vtGameIconSport}>
                    {ImageSrc(vtGameIconSport, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '5': //棋牌
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                staGameIconPokers?.map((staGameIconPoker, index) => {
                  return (<Option value={staGameIconPoker}>
                    {ImageSrc(staGameIconPoker, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            inputNode = <Select size='large' style={{ overflow: 'auto' }}>
              {
                vtGameIconPokers?.map((vtGameIconPoker, index) => {
                  return (<Option value={vtGameIconPoker}>
                    {ImageSrc(vtGameIconPoker, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '6': //捕魚
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                staGameIconFishings?.map((staGameIconFishing, index) => {
                  return (<Option value={staGameIconFishing}>
                    {ImageSrc(staGameIconFishing, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            inputNode = <Select size='large' style={{ overflow: 'auto' }}>
              {
                vtGameIconFishings?.map((vtGameIconFishing, index) => {
                  return (<Option value={vtGameIconFishing}>
                    {ImageSrc(vtGameIconFishing, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '7': //電競
        switch (agent) {
          case 'CF201':
          case 'STA01':
            break
          case 'VT999':
            inputNode = <Select size='large' style={{ overflow: 'auto' }}>
              {
                vtGameIconEsports?.map((vtGameIconEsport, index) => {
                  return (<Option value={vtGameIconEsport}>
                    {ImageSrc(vtGameIconEsport, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '8': //鬥雞
        switch (agent) {
          case 'CF201':
          case 'STA01':
            break
          case 'VT999':
            inputNode = <Select size='large' style={{ overflow: 'auto' }}>
              {
                vtGameIconCockfightings?.map((vtGameIconCockfighting, index) => {
                  return (<Option value={vtGameIconCockfighting}>
                    {ImageSrc(vtGameIconCockfighting, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], false, 100)}
                    </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      default:
        break
    }
  }

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: (title === 'gameProviderBgResIndex' || title === 'gameProviderDescription') ? false : true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

export default EditableCell