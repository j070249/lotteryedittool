import React, { useState, useEffect, useContext, useCallback } from 'react';
import { Table, Popconfirm, Form, Typography, Menu, Button, Upload } from 'antd';
import { UploadOutlined, DownloadOutlined, MenuOutlined } from '@ant-design/icons';
import Layout from 'antd/lib/layout/layout';
import EditableCell from './minePageEditableCell';
import ImageSrc from '../../components/imageSrc'
import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc';
import { arrayMoveImmutable } from 'array-move';
import '../../assets/css/sortable.css'
import * as fileApi from '../../services/fileApi'
import { StoreContext } from '../../provider/storeProvider'

const DragHandle = sortableHandle(() => <MenuOutlined style={{ cursor: 'grab', color: '#999' }} />);
const SortableItem = sortableElement(props => <tr {...props} />);
const SortableContainer = sortableContainer(props => <tbody {...props} />);

const MinePageJsonApply = (props) => {
  const [form] = Form.useForm();
  const [data, setData] = useState([]);
  const [editingKey, setEditingKey] = useState('');
  const [categoryCode, setCategoryCode] = useState('0')
  const [mineData, setMineData] = useState({})
  const [isLoading, setIsLoading] = useState(false)
  const [allData, setAllData] = useState([])
  const { urlState, agentState } = useContext(StoreContext);
  const [url] = urlState;
  const [agent, setAgent] = agentState;

  useEffect(() => {
    setAllData(props.data)
  }, [props.data])

  useEffect(() => {
    setEditingKey('')
  }, [agent, categoryCode])

  const getMineInfo = useCallback(() => {
    return mineData?.agent?.find((agentItem) =>
      agentItem?.code === agent
    )?.category?.find((typeItem) =>
      typeItem?.categoryCode?.toString() === categoryCode
    )?.categoryCenterList.map((item, index) => {
      item = {
        ...item,
      }
      return item
    })
  }, [agent, categoryCode, mineData])

  const mergeData = useCallback(() => {
    const data = allData?.find((item) => {
      return item?.categoryCode?.toString() === categoryCode
    })
    const newDatas = []
    data?.categoryCenterList?.forEach((categoryCenter, index) => {
      newDatas[index] = {
        entryId: categoryCenter.entryId,
        title: categoryCenter.title,
        iconRes: categoryCenter.iconRes,
        enable: false,
        key: index,
        index: index
      }
    })
    const mineInfos = getMineInfo()
    const value = newDatas?.map((newData, i) => {
      const _mineInfos = mineInfos?.find((mineInfo) => mineInfo.entryId === newData.entryId)
      newData = {
        ...newData,
        entryId: _mineInfos?.entryId,
        title: _mineInfos?.title,
        iconRes: _mineInfos?.iconRes,
        enable: _mineInfos?.enable,
      }
      return newData
    })
    setData(value)
  }, [allData, categoryCode, getMineInfo])

  useEffect(() => {
    mergeData()
  }, [mergeData])

  const syncFile = useCallback(() => {
    console.log('res', `${url}/static-files/${agent}/AppConfig/mine_centers.json`);
    const fetch = async () => {
      if (url === '') return
      const res = await fileApi.getMineFile(url, agent)
      setIsLoading(false)
      if (res === 'error') return
      setMineData(res?.data)
      console.log('data', res?.data);
    }
    fetch()
  }, [agent, url])

  useEffect(() => {
    setIsLoading(true)
    syncFile()
  }, [syncFile])

  const updateFile = async () => {
  }

  const updateLotteryInfo = (agent, type, data) => {
    const newData = []
    data.forEach((element, index) => {
      newData.push(
        {
          entryId: element.entryId,
          title: element.title,
          iconRes: element.iconRes,
          enable: element.enable
        }
      )
    });

    const agentIndex = mineData.agent.findIndex((agentItem) =>
      agentItem.code === agent
    )
    const typeIndex = mineData.agent[agentIndex].category.findIndex((typeItem) =>
      typeItem.categoryCode.toString() === type
    )
    var _mineData = mineData
    _mineData.agent[agentIndex].category[typeIndex].categoryCenterList = newData
    setMineData({ ..._mineData })
  }

  const isEditing = (record) => record.key === editingKey;
  const edit = (record) => {
    form.setFieldsValue({
      entryId: '',
      title: '',
      iconRes: '',
      enable: false,
      ...record,
    });
    setEditingKey(record.key);
  };

  const handleDelete = (key) => {
    const dataSource = [...data];
    const newData = dataSource.filter((item) => item.key !== key)
    setData(newData);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key) => {
    try {
      const row = await form.validateFields();
      const newData = [...data];
      const index = newData.findIndex((item) => key === item.key);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
      } else {
        newData.push(row);
      }
      updateLotteryInfo(agent, categoryCode, newData)
      setData(newData);
      setEditingKey('');
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const handleItemClick = e => {
    setCategoryCode(e.key);
  };

  const handleAdd = e => {
    const data = allData?.map((item) => {
      if (item?.categoryCode?.toString() === categoryCode) {
        item.categoryCenterList.push(
          {
            entryId: '',
            title: '',
            iconRes: '',
            enable: false
          }
        )
        return item
      }
      return item
    })
    setAllData(data)
  };

  const columns = [
    {
      title: 'sort',
      dataIndex: 'sort',
      width: 60,
      className: 'drag-visible',
      render: () => <div style={{ display: 'flex', justifyContent: 'center' }}>{<DragHandle />}</div>,
    },
    {
      title: 'entryId',
      dataIndex: 'entryId',
      width: 100,
      editable: true,
    },
    {
      title: 'title',
      dataIndex: 'title',
      width: 100,
      editable: true,
    },
    {
      title: 'iconRes',
      dataIndex: 'iconRes',
      width: 150,
      editable: true,
      render: (img, record) => {
        return ImageSrc(img, null, true)
      }
    },
    {
      title: 'enable',
      dataIndex: 'enable',
      width: 160,
      editable: true,
      render: bool => <div >{bool ? 'true' : 'false'}</div>,
    },
    {
      title: 'operation',
      dataIndex: 'operation',
      width: 150,
      fixed: 'right',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.key)} style={{ marginRight: 8, }}>
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <Typography.Link>Cancel</Typography.Link>
            </Popconfirm>
          </span>
        ) : (
          <span>
            <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)} style={{ marginRight: 8, }}>
              Edit
            </Typography.Link>
            <Popconfirm disabled={editingKey !== ''} title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
              <Typography.Link disabled={editingKey !== ''} >Delete</Typography.Link>
            </Popconfirm>
          </span>
        );
      },
    },
  ];
  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return {
        ...col,
        onCell: (record) => ({
          style: {
            backgroundColor: (!record.enable === true) ? '#dcdcdc' : ''
          }
        }),
      };
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex === 'code' ? 'number' : 'text',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
        categoryCode: categoryCode,
        agent: agent,
        style: {
          backgroundColor: (!record.enable === true) ? '#dcdcdc' : ''
        }
      }),
    };
  });

  const onSortEnd = ({ oldIndex, newIndex }) => {
    if (oldIndex !== newIndex) {
      const newData = arrayMoveImmutable([].concat(data), oldIndex, newIndex).filter(el => !!el);
      updateLotteryInfo(agent, categoryCode, newData)
      setData(newData)
    }
  };

  const DraggableContainer = props => (
    <SortableContainer
      useDragHandle
      helperClass="row-dragging"
      onSortEnd={onSortEnd}
      {...props}
    />
  );

  const DraggableBodyRow = ({ className, style, ...restProps }) => {
    const index = data?.findIndex(x => x.index === restProps['data-row-key']);
    return <SortableItem index={index} {...restProps} />;
  };


  return (
    <Layout>

      <div style={{ flexDirection: 'row', marginTop: 10 }}>
        <Upload
          accept=".json"
          showUploadList={false}
          beforeUpload={(file, fileList) => {
            const reader = new FileReader();

            reader.onload = e => {
              setMineData(JSON.parse(e.target.result))
            };

            reader.readAsText(file);

            return false;
          }}
        >
          <Button icon={<UploadOutlined />}
            style={{ width: 120 }}>
            Upload
          </Button>
        </Upload>

        <Button
          href={`data:text/json;charset=utf-8,${encodeURIComponent(
            JSON.stringify({
              agent: mineData?.agent?.filter((agentItem) =>
                agentItem?.code === agent
              )
            })
          )}`}
          download="mine_centers.json"
          icon={<DownloadOutlined />}
          style={{ width: 120 }}>
          Download
        </Button>

        <Button
          icon={<UploadOutlined />}
          onClick={updateFile}
          style={{ width: 120 }}
          disabled>
          上傳
        </Button>
      </div>

      <Button
        type="primary"
        onClick={(e) => handleAdd(e)}
        style={{
          marginTop: 16,
          marginBottom: 16,
          width: 100
        }}
      >
        Add a row
      </Button>

      <Menu
        defaultSelectedKeys={['1']}
        onClick={(e) => handleItemClick(e)}
        selectedKeys={[categoryCode]}
        mode="horizontal"
      >
        {
          allData?.map((element, index) => {
            return (<Menu.Item key={element.categoryCode}>
              {element.categoryName}
            </Menu.Item>)
          })
        }
      </Menu>

      <Form form={form} component={false}>
        <Table
          components={{
            body: {
              cell: EditableCell,
              wrapper: DraggableContainer,
              row: DraggableBodyRow
            },
          }}
          bordered
          scroll={{ y: '60vh' }}
          dataSource={data}
          columns={mergedColumns}
          rowClassName="editable-row"
          pagination={false}
          // pagination={{
          //   onChange: cancel,
          // }}
          sticky
          loading={isLoading}
        />
      </Form>
    </Layout>
  );
};

export default MinePageJsonApply
