import { Input, Select, Form } from 'antd';
import ImageSrc from '../../components/imageSrc'

const { Option } = Select;

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  categoryCode,
  agent,
  ...restProps
}) => {
  var inputNode = <Input />

  const staRows = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_charge.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_withdraw.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_transfer.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_vip_detail.png'
  ]

  const vtRows = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_charge.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_withdraw.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_transfer.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_vip_detail.png'
  ]

  const staPersonals = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_recharge.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_betting.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_chase.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_withdraw.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_ransfer.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_switch.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_profit.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_setting.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_bank_card.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_letter.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_crown.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_referral.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/call_center_agent.png',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_update.webp'
  ]

  const vtPersonals = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_recharge.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_betting.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_chase.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_withdraw.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_ransfer.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_switch.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_history_profit.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_setting.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_bank_card.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_letter.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_crown.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_referral.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/call_center_agent.png',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/ic_update.webp'
  ]

  const staProxys = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_profile.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_ranking.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_account.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_members.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_profit.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_bonus.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_day_wage.png',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_day_dividend.png',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_month_dividend.png'
  ]

  const vtProxys = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_profile.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_ranking.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_account.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_members.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_profit.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_team_bonus.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_day_wage.png',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_day_dividend.png',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_month_dividend.png'
  ]

  if (title === 'enable') {
    inputNode = <Select>
      <Option value={false}>false</Option>
      <Option value={true}>true</Option>
    </Select>
  } else if (title === 'iconRes') {
    switch (categoryCode) {
      case '0': //橫列
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                staRows?.map((value, index) => {
                  return (<Option value={value}>
                    {ImageSrc(value, null, false, 100)}
                  </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                vtRows?.map((value, index) => {
                  return (<Option value={value}>
                    {ImageSrc(value, null, false, 100)}
                  </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '1': //个人中心
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                staPersonals?.map((value, index) => {
                  return (<Option value={value}>
                    {ImageSrc(value, null, false, 100)}
                  </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                vtPersonals?.map((value, index) => {
                  return (<Option value={value}>
                    {ImageSrc(value, null, false, 100)}
                  </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      case '2': //代理中心
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                staProxys?.map((value, index) => {
                  return (<Option value={value}>
                    {ImageSrc(value, null, false, 100)}
                  </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                vtProxys?.map((value, index) => {
                  return (<Option value={value}>
                    {ImageSrc(value, null, false, 100)}
                  </Option>
                  )
                })
              }
            </Select>
            break
          default:
            break
        }
        break
      default:
        break
    }
  }

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

export default EditableCell