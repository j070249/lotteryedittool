import React, { useState, useEffect, useContext, useCallback } from 'react';
import { Table, Popconfirm, Form, Typography, Menu, Button, Upload } from 'antd';
import { UploadOutlined, DownloadOutlined, MenuOutlined } from '@ant-design/icons';
import Layout from 'antd/lib/layout/layout';
import EditableCell from './thirdPartyGameEditableCell'
import ImageSrc from '../../components/imageSrc'
import ThirdPartyGameStyle from '../../assets/styles/thirdPartyGameStyle'
import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc';
import { arrayMoveImmutable } from 'array-move';
import '../../assets/css/sortable.css'
import * as fileApi from '../../services/fileApi'
import { StoreContext } from '../../provider/storeProvider'

const DragHandle = sortableHandle(() => <MenuOutlined style={{ cursor: 'grab', color: '#999' }} />);
const SortableItem = sortableElement(props => <tr {...props} />);
const SortableContainer = sortableContainer(props => <tbody {...props} />);

const ThirdPartyGameJsonApply = (props) => {
	const [form] = Form.useForm();
	const [data, setData] = useState([]);
	const [editingKey, setEditingKey] = useState('');
	const [categoryCode, setCategoryCode] = useState('2')
	const [thirdPartyGameData, setThirdPartyGameData] = useState({})
	const [isLoading, setIsLoading] = useState(false)
	const [allData, setAllData] = useState([])
	const { urlState, agentState } = useContext(StoreContext);
	const [url] = urlState;
	const [agent, setAgent] = agentState;

	useEffect(() => {
		setAllData(props.data)
	}, [props.data])

	useEffect(() => {
		setEditingKey('')
	}, [agent, categoryCode])

	const getCategoryGameList = useCallback(() => {
		return thirdPartyGameData?.agent?.find((agentItem) =>
			agentItem?.code === agent
		)?.category?.find((typeItem) =>
			typeItem?.categoryCode?.toString() === categoryCode
		)?.categoryGameList.map((item, index) => {
			item = {
				...item,
			}
			return item
		})
	}, [agent, categoryCode, thirdPartyGameData])

	const mergeData = useCallback(() => {
		const data = allData
		const newDatas = []
		var itemIndex = 0
		data?.forEach((thirdParty, index) => {
			thirdParty?.gameLobbys?.forEach((game, gameIndex) => {
				newDatas[itemIndex] = {
					gameProviderId: thirdParty.gameProviderId,
					gameProviderCode: thirdParty.gameProviderCode,
					gameProviderName: game.description,
					gameProviderTypeId: game.gameProviderTypeId,
					gameProviderTypeCode: game.gameProviderTypeCode,
					gameProviderIconName: '',
					gameProviderBgResIndex: '',
					gameProviderDescription: '',
					isLobbyEntry: '',
					isSupportDesktopWeb: '',
					Enabled: false,
					key: itemIndex,
					index: itemIndex
				}
				itemIndex ++
			})
		})
		//console.log('newDatas', newDatas)
		const categoryGameList = getCategoryGameList()
		//console.log('categoryGameList', categoryGameList)
		const value = newDatas?.map((newData, i) => {
			const _categoryGameList = categoryGameList?.find((categoryGame) => 
			categoryGame.gameProviderId === newData.gameProviderId && categoryGame.gameProviderTypeId === newData.gameProviderTypeId)
			newData = {
				...newData,
				gameProviderIconName: _categoryGameList?.gameProviderIconName,
				gameProviderBgResIndex: _categoryGameList?.gameProviderBgResIndex,
				gameProviderDescription: _categoryGameList?.gameProviderDescription,
				isLobbyEntry: _categoryGameList?.isLobbyEntry,
				isSupportDesktopWeb: _categoryGameList?.isSupportDesktopWeb,
				Enabled: _categoryGameList ? true : false
			}
			return newData
		})
		setData(value)
	}, [allData, categoryCode, getCategoryGameList])

	useEffect(() => {
		mergeData()
	}, [mergeData])

	const syncFile = useCallback(() => {
		const fetch = async () => {
			if (url === '') return
			const res = await fileApi.getThirdPartyGameFile(url, agent)
			setIsLoading(false)
			if (res === 'error') return
			setThirdPartyGameData(res?.data)
		}
		fetch()
	}, [agent, url])

	useEffect(() => {
		setIsLoading(true)
		syncFile()
	}, [syncFile])

	const updateFile = async () => {
	}

	const updateCategoryGameList = (agent, type, data) => {
		const newData = []
		data.forEach((element, index) => {
			if (element.Enabled)
				newData.push(
					{
						gameProviderId: element.gameProviderId,
						gameProviderCode: element.gameProviderCode,
						gameProviderName: element.gameProviderName,
						gameProviderTypeId: element.gameProviderTypeId,
						gameProviderTypeCode: element.gameProviderTypeCode,
						gameProviderIconName: element.gameProviderIconName,
						gameProviderBgResIndex: element.gameProviderBgResIndex,
						gameProviderDescription: element.gameProviderDescription,
						isLobbyEntry: element.isLobbyEntry,
						isSupportDesktopWeb: element.isSupportDesktopWeb,
					}
				)
		});

		const agentIndex = thirdPartyGameData.agent.findIndex((agentItem) =>
			agentItem.code === agent
		)
		const typeIndex = thirdPartyGameData.agent[agentIndex].category.findIndex((typeItem) =>
			typeItem.categoryCode.toString() === type
		)
		var _thirdPartyGameData = thirdPartyGameData
		_thirdPartyGameData.agent[agentIndex].category[typeIndex].categoryGameList = newData
		setThirdPartyGameData(_thirdPartyGameData)
	}

	const isEditing = (record) => record.key === editingKey;
	const edit = (record) => {
		form.setFieldsValue({
			gameProviderId: '',
			gameProviderCode: '',
			gameProviderName: '',
			gameProviderTypeId: '',
			gameProviderTypeCode: '',
			gameProviderIconName: '',
			gameProviderBgResIndex: '',
			gameProviderDescription: '',
			isLobbyEntry: '',
			isSupportDesktopWeb: '',
			Enabled: '',
			...record,
		});
		setEditingKey(record.key);
	};

	const handleDelete = (key) => {
		const dataSource = [...data];
		const newData = dataSource.filter((item) => item.key !== key)
		setData(newData);
	};

	const cancel = () => {
		setEditingKey('');
	};

	const save = async (key) => {
		try {
			const row = await form.validateFields();
			const newData = [...data];
			const index = newData.findIndex((item) => key === item.key);

			if (index > -1) {
				const item = newData[index];
				newData.splice(index, 1, { ...item, ...row });
			} else {
				newData.push(row);
			}
			updateCategoryGameList(agent, categoryCode, newData)
			setData(newData);
			setEditingKey('');
		} catch (errInfo) {
			console.log('Validate Failed:', errInfo);
		}
	};

	const handleItemClick = e => {
		setCategoryCode(e.key);
	};

	const handleAdd = e => {
		const data = allData?.push(
			{
				gameProviderId: '',
				gameProviderCode: '',
				gameProviderName: '',
			}
		)
		setAllData(data)
	};

	const columns = [
		{
			title: 'sort',
			dataIndex: 'sort',
			width: 60,
			className: 'drag-visible',
			render: () => <div style={{ display: 'flex', justifyContent: 'center' }}>{<DragHandle />}</div>,
		},
		{
			title: 'gameProviderId',
			dataIndex: 'gameProviderId',
			width: 150,
			editable: false,
		},
		{
			title: 'gameProviderCode',
			dataIndex: 'gameProviderCode',
			width: 180,
			editable: false,
		},
		{
			title: 'gameProviderName',
			dataIndex: 'gameProviderName',
			width: 180,
			editable: false,
		},
		{
			title: 'gameProviderTypeId',
			dataIndex: 'gameProviderTypeId',
			width: 180,
			editable: false,
		},
		{
			title: 'gameProviderTypeCode',
			dataIndex: 'gameProviderTypeCode',
			width: 200,
			editable: false,
		},
		{
			title: 'gameProviderBgResIndex',
			dataIndex: 'gameProviderBgResIndex',
			width: 210,
			editable: true,
			render: index => {
				return <div style={{ width: 100, height: 100, background: ThirdPartyGameStyle[agent][index] }}>{index}</div>
			}
		},
		{
			title: 'gameProviderIconName',
			dataIndex: 'gameProviderIconName',
			width: 200,
			editable: true,
			render: (img, record) => {
				return ImageSrc(img, ThirdPartyGameStyle[agent][record.gameProviderBgResIndex], true, (agent === 'VT999') ? 160 : 100)
			}
		},
		{
			title: 'gameProviderDescription',
			dataIndex: 'gameProviderDescription',
			width: 210,
			editable: true,
		},
		{
			title: 'isLobbyEntry',
			dataIndex: 'isLobbyEntry',
			width: 150,
			editable: true,
			render: bool => <div >{bool ? 'true' : 'false'}</div>,
		},
		{
			title: 'isSupportDesktopWeb',
			dataIndex: 'isSupportDesktopWeb',
			width: 200,
			editable: true,
			render: bool => <div >{bool ? 'true' : 'false'}</div>,
		},
		{
			title: 'Enabled',
			dataIndex: 'Enabled',
			width: 160,
			editable: true,
			render: bool => <div >{bool ? 'true' : 'false'}</div>,
		},
		{
			title: 'operation',
			dataIndex: 'operation',
			width: 150,
			fixed: 'right',
			render: (_, record) => {
				const editable = isEditing(record);
				return editable ? (
					<span>
						<Typography.Link
							onClick={() => save(record.key)} style={{ marginRight: 8, }}>
							Save
						</Typography.Link>
						<Popconfirm title="Sure to cancel?" onConfirm={cancel}>
							<Typography.Link>Cancel</Typography.Link>
						</Popconfirm>
					</span>
				) : (
					<span>
						<Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)} style={{ marginRight: 8, }}>
							Edit
						</Typography.Link>
						<Popconfirm disabled={editingKey !== ''} title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
							<Typography.Link disabled={editingKey !== ''} >Delete</Typography.Link>
						</Popconfirm>
					</span>
				);
			},
		},
	];
	const mergedColumns = columns.map((col) => {
		if (!col.editable) {
			return {
				...col,
				onCell: (record) => ({
					style: {
						backgroundColor: (!record.Enabled === true) ? '#dcdcdc' : ''
					}
				}),
			};
		}

		return {
			...col,
			onCell: (record) => ({
				record,
				inputType: col.dataIndex === 'gameProviderId' ? 'number' : 'text',
				dataIndex: col.dataIndex,
				title: col.title,
				editing: isEditing(record),
				categoryCode: categoryCode,
				agent: agent,
				style: {
					backgroundColor: (!record.Enabled === true) ? '#dcdcdc' : ''
				}
			}),
		};
	});

	const onSortEnd = ({ oldIndex, newIndex }) => {
		if (oldIndex !== newIndex) {
			const newData = arrayMoveImmutable([].concat(data), oldIndex, newIndex).filter(el => !!el);
			updateCategoryGameList(agent, categoryCode, newData)
			setData(newData)
		}
	};

	const DraggableContainer = props => (
		<SortableContainer
			useDragHandle
			helperClass="row-dragging"
			onSortEnd={onSortEnd}
			{...props}
		/>
	);

	const DraggableBodyRow = ({ className, style, ...restProps }) => {
		const index = data?.findIndex(x => x.index === restProps['data-row-key']);
		return <SortableItem index={index} {...restProps} />;
	};

	return (
		<Layout>

			<div style={{ flexDirection: 'row', marginTop: 10 }}>
				<Upload
					accept=".json"
					showUploadList={false}
					beforeUpload={(file, fileList) => {
						const reader = new FileReader();

						reader.onload = e => {
							setThirdPartyGameData(JSON.parse(e.target.result))
						};

						reader.readAsText(file);

						return false;
					}}
				>
					<Button icon={<UploadOutlined />}
						style={{ width: 120 }}>
						Upload
					</Button>
				</Upload>
				<Button
					href={`data:text/json;charset=utf-8,${encodeURIComponent(
						JSON.stringify({
							agent: thirdPartyGameData?.agent?.filter((agentItem) =>
								agentItem?.code === agent
							)
						})
					)}`}
					download="third_party_game.json"
					icon={<DownloadOutlined />}
					style={{ width: 120 }}>
					Download
				</Button>

				<Button
					icon={<UploadOutlined />}
					onClick={updateFile}
					style={{ width: 120 }}
					disabled>
					上傳
				</Button>
			</div>

			<Button
				type="primary"
				onClick={(e) => handleAdd(e)}
				style={{
					marginTop: 16,
					marginBottom: 16,
					width: 100
				}}
			>
				Add a row
			</Button>

			<Menu
				defaultSelectedKeys={['1']}
				onClick={(e) => handleItemClick(e)}
				selectedKeys={[categoryCode]}
				mode="horizontal">

				<Menu.Item key="0" disabled>
					首頁
				</Menu.Item>
				<Menu.Item key="1" disabled={(agent === 'VT999') ? false : true}>
					彩票
				</Menu.Item>
				<Menu.Item key="2">
					視訊
				</Menu.Item>
				<Menu.Item key="3">
					電子
				</Menu.Item>
				<Menu.Item key="4">
					體育
				</Menu.Item>
				<Menu.Item key="5">
					棋牌
				</Menu.Item>
				<Menu.Item key="6">
					捕魚
				</Menu.Item>
				<Menu.Item key="7" disabled>
					電競
				</Menu.Item>
				<Menu.Item key="8" disabled={(agent === 'VT999') ? false : true}>
					鬥雞
				</Menu.Item>

				{
					// allData?.map((element, index) => {
					// 	return (<Menu.Item key={element.typeId}>
					// 		{element.typeName}
					// 	</Menu.Item>)
					// })
				}
			</Menu>

			<Form form={form} component={false}>
				<Table
					components={{
						body: {
							cell: EditableCell,
							wrapper: DraggableContainer,
							row: DraggableBodyRow
						},
					}}
					bordered
					scroll={{ y: '60vh' }}
					dataSource={data}
					columns={mergedColumns}
					rowClassName="editable-row"
					pagination={{
						onChange: cancel,
					}}
					loading={isLoading}
				/>
			</Form>
		</Layout>
	);
};

export default ThirdPartyGameJsonApply
