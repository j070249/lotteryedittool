import { Input, Select, Form, Upload, Divider } from 'antd';
import ImageSrc from '../../components/imageSrc'
import LotteryColorStyle from '../../assets/styles/lotteryColorStyle'
import ImgCrop from 'antd-img-crop';

const { Option } = Select;

const onPreview = async file => {
  let src = file.url;
  if (!src) {
    src = await new Promise(resolve => {
      const reader = new FileReader();
      reader.readAsDataURL(file.originFileObj);
      reader.onload = () => resolve(reader.result);
    });
  }
  const image = new Image();
  image.src = src;
  const imgWindow = window.open(src);
  imgWindow.document.write(image.outerHTML);
};

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  lotteryTypeCode,
  agent,
  ...restProps
}) => {
  var inputNode = <Input />

  const stalotteryIconSscs = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_xinjiang.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_tianjin.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_s_5.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_s_2.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_s_1.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_chongqing.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lobby_lottery_taiwan_jc_small.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lobby_lottery_slovak_keno_small.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lobby_lottery_canada_keno_small.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lobby_lottery_australia_jc_small.webp'
  ]

  const stalotteryIcon115s = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lobby_lottery_taiwan_11_5_small.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lobby_lottery_slovak_11_5_small.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lobby_lottery_australia_11_5_small.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lobby_lottery_canada_11_5_small.webp'
  ]

  const stalotteryIconK3s = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_dice_5.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_dice_4.webp',
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_dice_3.webp'
  ]

  const stalotteryIconMark6s = [
    'https://daplstgcommon101.blob.core.windows.net/static-files/images-src/icon_lottery_marksix.webp'
  ]

  if (title === 'random') {
    inputNode = <Select>
      <Option value={0}>無</Option>
      <Option value={1}>有</Option>
    </Select>
  } else if (title === 'supportLotterySource' | title === 'supportLotteryTrend' | title === 'supportLotteryTrace' || title === 'Enabled') {
    inputNode = <Select>
      <Option value={false}>false</Option>
      <Option value={true}>true</Option>
    </Select>
  } else if (title === 'colorIndex') {
    var indents = [];
    for (var i = 0; i < 8; i++) {
      indents.push(<Option key={i} value={i}>{<div style={{ width: 100, height: 100, background: LotteryColorStyle[agent][i] }}>{i}</div>}</Option>);
    }
    inputNode = <Select>
      {indents}
    </Select>
  } else if (title === 'img') {
    switch (lotteryTypeCode) {
      case '1': //時時彩
        switch (agent) {
          case 'CF201':
          case 'STA01':

            inputNode = <Select
              size='large'
              className={'myselectstyle'}
              dropdownRender={menu => (
                <div>
                  {menu}
                  <Divider style={{ margin: '4px 0' }} />
                  <div style={{ display: 'flex', flexWrap: 'nowrap', padding: 8 }}>
                    <ImgCrop rotate>
                      <Upload
                        style={{ marginLeft: 8, margin: 8 }}
                        action=""
                        listType="picture-card"
                        onPreview={onPreview}
                      >
                        {'+ Upload'}
                      </Upload>
                    </ImgCrop>
                  </div>
                </div>
              )}>
              {
                stalotteryIconSscs?.map((stalotteryIconSsc, index) => {
                  return (<Option value={stalotteryIconSsc}>
                    {ImageSrc(stalotteryIconSsc, LotteryColorStyle[agent][record.colorIndex])}
                    </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            break
          default:
            break
        }
        break
      case '6': //11選5
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                stalotteryIcon115s?.map((stalotteryIcon115, index) => {
                  return (<Option value={stalotteryIcon115}>
                    {ImageSrc(stalotteryIcon115, LotteryColorStyle[agent][record.colorIndex])}
                    </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            break
          default:
            break
        }
        break
      case '4': //快3
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                stalotteryIconK3s?.map((stalotteryIconK3, index) => {
                  return (<Option value={stalotteryIconK3}>
                    {ImageSrc(stalotteryIconK3, LotteryColorStyle[agent][record.colorIndex])}
                    </Option>
                  )
                })
              }
            </Select>
            break
          case 'VT999':
            break
          default:
            break
        }
        break
      case '7': //PK10
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
            </Select>
            break
          case 'VT999':
            break
          default:
            break
        }
        break
      case '10': //六合彩
        switch (agent) {
          case 'CF201':
          case 'STA01':
            inputNode = <Select size='large' className={'myselectstyle'}>
              {
                stalotteryIconMark6s?.map((stalotteryIconMark6, index) => {
                  return (<Option value={stalotteryIconMark6}>
                    {ImageSrc(stalotteryIconMark6, LotteryColorStyle[agent][record.colorIndex])}
                    </Option>
                  )
                })
              }            
              </Select>
            break
          case 'VT999':
            break
          default:
            break
        }
        break
      default:
        break
    }
  }

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

export default EditableCell