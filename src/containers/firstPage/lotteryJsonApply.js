import React, { useState, useEffect, useContext, useCallback } from 'react';
import { Table, Popconfirm, Form, Typography, Menu, Button, Upload } from 'antd';
import { UploadOutlined, DownloadOutlined, MenuOutlined } from '@ant-design/icons';
import Layout from 'antd/lib/layout/layout';
import EditableCell from './lotteryEditableCell';
import ImageSrc from '../../components/imageSrc'
import LotteryColorStyle from '../../assets/styles/lotteryColorStyle'
import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc';
import { arrayMoveImmutable } from 'array-move';
import '../../assets/css/sortable.css'
import * as fileApi from '../../services/fileApi'
import { StoreContext } from '../../provider/storeProvider'

const DragHandle = sortableHandle(() => <MenuOutlined style={{ cursor: 'grab', color: '#999' }} />);
const SortableItem = sortableElement(props => <tr {...props} />);
const SortableContainer = sortableContainer(props => <tbody {...props} />);

const LotteryJsonApply = (props) => {
  const [form] = Form.useForm();
  const [data, setData] = useState([]);
  const [editingKey, setEditingKey] = useState('');
  const [lotteryTypeCode, setLotteryTypeCode] = useState('1')
  const [lotteryData, setLotteryData] = useState({})
  const [isLoading, setIsLoading] = useState(false)
  const [allData, setAllData] = useState([])
  const { urlState, agentState } = useContext(StoreContext);
  const [url] = urlState;
  const [agent, setAgent] = agentState;

  useEffect(() => {
    setAllData(props.data)
  }, [props.data])

  useEffect(() => {
    setEditingKey('')
  }, [agent, lotteryTypeCode])

  const getLotteryInfo = useCallback(() => {
    return lotteryData?.agent?.find((agentItem) =>
      agentItem?.code === agent
    )?.category?.find((typeItem) =>
      typeItem?.lotteryTypeCode?.toString() === lotteryTypeCode
    )?.lotteryInfo.map((item, index) => {
      item = {
        ...item,
        // "key": index,
        // "index": index
      }
      return item
    })
  }, [agent, lotteryTypeCode, lotteryData])

  const mergeData = useCallback(() => {
    const data = allData?.find((item) => {
      return item?.typeId?.toString() === lotteryTypeCode
    })
    const newDatas = []
    data?.lotteries?.forEach((lottery, index) => {
      newDatas[index] = {
        code: lottery.code,
        name: lottery.codeName,
        desc: lottery.name,
        colorIndex: '',
        img: '',
        random: '',
        supportLotterySource: '',
        supportLotteryTrend: '',
        supportLotteryTrace: '',
        Enabled: false,
        key: index,
        index: index
      }
    })
    const lotteryInfos = getLotteryInfo()
    const value = newDatas?.map((newData, i) => {
      const _lotteryInfo = lotteryInfos?.find((lotteryInfo) => lotteryInfo.code === newData.code)
      newData = {
        ...newData,
        colorIndex: _lotteryInfo?.colorIndex,
        img: _lotteryInfo?.img,
        random: _lotteryInfo?.random,
        supportLotterySource: _lotteryInfo?.supportLotterySource,
        supportLotteryTrend: _lotteryInfo?.supportLotteryTrend,
        supportLotteryTrace: _lotteryInfo?.supportLotteryTrace,
        Enabled: _lotteryInfo ? true : false
      }
      return newData
    })
    setData(value)
  }, [allData, lotteryTypeCode, getLotteryInfo])

  useEffect(() => {
    mergeData()
  }, [mergeData])

  const syncFile = useCallback(() => {
    const fetch = async () => {
      if (url === '') return
      const res = await fileApi.getLotteryFile(url, agent)
      setIsLoading(false)
      if (res === 'error') return
      setLotteryData(res?.data)
    }
    fetch()
  }, [agent, url])

  useEffect(() => {
    setIsLoading(true)
    syncFile()
  }, [syncFile])

  const updateFile = async () => {
  }

  const updateLotteryInfo = (agent, type, data) => {
    const newData = []
    data.forEach((element, index) => {
      if (element.Enabled)
        newData.push(
          {
            code: element.code,
            name: element.name,
            desc: element.desc,
            colorIndex: element.colorIndex,
            img: element.img,
            random: element.random,
            supportLotterySource: element.supportLotterySource,
            supportLotteryTrend: element.supportLotteryTrend,
            supportLotteryTrace: element.supportLotteryTrace
          }
        )
    });

    const agentIndex = lotteryData.agent.findIndex((agentItem) =>
      agentItem.code === agent
    )
    const typeIndex = lotteryData.agent[agentIndex].category.findIndex((typeItem) =>
      typeItem.lotteryTypeCode.toString() === type
    )
    var _lotteryData = lotteryData
    _lotteryData.agent[agentIndex].category[typeIndex].lotteryInfo = newData
    setLotteryData({ ..._lotteryData })
  }

  const isEditing = (record) => record.key === editingKey;
  const edit = (record) => {
    form.setFieldsValue({
      code: '',
      name: '',
      desc: '',
      colorIndex: '',
      img: '',
      random: '',
      supportLotterySource: '',
      supportLotteryTrend: '',
      supportLotteryTrace: '',
      Enabled: '',
      ...record,
    });
    setEditingKey(record.key);
  };

  const handleDelete = (key) => {
    const dataSource = [...data];
    const newData = dataSource.filter((item) => item.key !== key)
    // updateLotteryInfo(agent, lotteryTypeCode, newData)
    setData(newData);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key) => {
    try {
      const row = await form.validateFields();
      const newData = [...data];
      const index = newData.findIndex((item) => key === item.key);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
      } else {
        newData.push(row);
      }
      updateLotteryInfo(agent, lotteryTypeCode, newData)
      setData(newData);
      setEditingKey('');
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const handleItemClick = e => {
    setLotteryTypeCode(e.key);
  };

  const handleAdd = e => {
    const data = allData?.map((item) => {
      if (item?.typeId?.toString() === lotteryTypeCode) {
        item.lotteries.push(
          {
            code: '',
            name: '',
            desc: '',
            colorIndex: '',
            img: '',
            random: '',
            supportLotterySource: '',
            supportLotteryTrend: '',
            supportLotteryTrace: '',
            Enabled: false,
          }
        )
        return item
      }
      return item
    })
    setAllData(data)
  };

  const columns = [
    {
      title: 'sort',
      dataIndex: 'sort',
      width: 60,
      className: 'drag-visible',
      render: () => <div style={{ display: 'flex', justifyContent: 'center' }}>{<DragHandle />}</div>,
    },
    {
      title: 'code',
      dataIndex: 'code',
      width: 100,
      editable: false,
    },
    {
      title: 'name',
      dataIndex: 'name',
      width: 100,
      editable: false,
    },
    {
      title: 'desc',
      dataIndex: 'desc',
      width: 100,
      editable: false,
    },
    {
      title: 'colorIndex',
      dataIndex: 'colorIndex',
      width: 150,
      editable: true,
      render: index => {
        return <div style={{ width: 100, height: 100, background: LotteryColorStyle[agent][index] }}>{index}</div>
      }
    },
    {
      title: 'img',
      dataIndex: 'img',
      width: 150,
      editable: true,
      render: (img, record) => {
        return ImageSrc(img, LotteryColorStyle[agent][record.colorIndex], true)
      }
    },
    {
      title: 'random',
      dataIndex: 'random',
      width: 100,
      editable: true,
      render: v => <div >{v === 1 ? '有' : '無'}</div>,
    },
    {
      title: 'supportLotterySource',
      dataIndex: 'supportLotterySource',
      width: 160,
      editable: true,
      render: bool => <div >{bool ? 'true' : 'false'}</div>,
    },
    {
      title: 'supportLotteryTrend',
      dataIndex: 'supportLotteryTrend',
      width: 160,
      editable: true,
      render: bool => <div >{bool ? 'true' : 'false'}</div>,
    },
    {
      title: 'supportLotteryTrace',
      dataIndex: 'supportLotteryTrace',
      width: 160,
      editable: true,
      render: bool => <div >{bool ? 'true' : 'false'}</div>,
    },
    {
      title: 'Enabled',
      dataIndex: 'Enabled',
      width: 160,
      editable: true,
      render: bool => <div >{bool ? 'true' : 'false'}</div>,
    },
    {
      title: 'operation',
      dataIndex: 'operation',
      width: 150,
      fixed: 'right',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.key)} style={{ marginRight: 8, }}>
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <Typography.Link>Cancel</Typography.Link>
            </Popconfirm>
          </span>
        ) : (
          <span>
            <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)} style={{ marginRight: 8, }}>
              Edit
            </Typography.Link>
            <Popconfirm disabled={editingKey !== ''} title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
              <Typography.Link disabled={editingKey !== ''} >Delete</Typography.Link>
            </Popconfirm>
          </span>
        );
      },
    },
  ];
  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return {
        ...col,
        onCell: (record) => ({
          style: {
            backgroundColor: (!record.Enabled === true ) ? '#dcdcdc' : ''
          }
        }),
      };
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex === 'code' ? 'number' : 'text',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
        lotteryTypeCode: lotteryTypeCode,
        agent: agent,
        style: {
          backgroundColor: (!record.Enabled === true) ? '#dcdcdc' : ''
        }
      }),
    };
  });

  const onSortEnd = ({ oldIndex, newIndex }) => {
    if (oldIndex !== newIndex) {
      const newData = arrayMoveImmutable([].concat(data), oldIndex, newIndex).filter(el => !!el);
      updateLotteryInfo(agent, lotteryTypeCode, newData)
      setData(newData)
    }
  };

  const DraggableContainer = props => (
    <SortableContainer
      useDragHandle
      helperClass="row-dragging"
      onSortEnd={onSortEnd}
      {...props}
    />
  );

  const DraggableBodyRow = ({ className, style, ...restProps }) => {
    const index = data?.findIndex(x => x.index === restProps['data-row-key']);
    return <SortableItem index={index} {...restProps} />;
  };


  return (
    <Layout>

      <div style={{ flexDirection: 'row', marginTop: 10 }}>
        <Upload
          accept=".json"
          showUploadList={false}
          beforeUpload={(file, fileList) => {
            const reader = new FileReader();

            reader.onload = e => {
              setLotteryData(JSON.parse(e.target.result))
            };

            reader.readAsText(file);

            return false;
          }}
        >
          <Button icon={<UploadOutlined />}
            style={{ width: 120 }}>
            Upload
          </Button>
        </Upload>

        <Button
          href={`data:text/json;charset=utf-8,${encodeURIComponent(
            JSON.stringify({
              agent: lotteryData?.agent?.filter((agentItem) =>
                agentItem?.code === agent
              )
            })
          )}`}
          download="lottery.json"
          icon={<DownloadOutlined />}
          style={{ width: 120 }}>
          Download
        </Button>

        <Button
          icon={<UploadOutlined />}
          onClick={updateFile}
          style={{ width: 120 }}
          disabled>
          上傳
        </Button>
      </div>

      <Button
        type="primary"
        onClick={(e) => handleAdd(e)}
        style={{
          marginTop: 16,
          marginBottom: 16,
          width: 100
        }}
      >
        Add a row
      </Button>

      <Menu
        defaultSelectedKeys={['1']}
        onClick={(e) => handleItemClick(e)}
        selectedKeys={[lotteryTypeCode]}
        mode="horizontal"
      >
        {
          allData?.map((element, index) => {
            return (<Menu.Item key={element.typeId}>
              {element.typeName}
            </Menu.Item>)
          })
        }
      </Menu>

      <Form form={form} component={false}>
        <Table
          components={{
            body: {
              cell: EditableCell,
              wrapper: DraggableContainer,
              row: DraggableBodyRow
            },
          }}
          bordered
          scroll={{ y: '60vh' }}
          dataSource={data}
          columns={mergedColumns}
          rowClassName="editable-row"
          pagination={{
            onChange: cancel,
          }}
          sticky
          loading={isLoading}
        />
      </Form>
    </Layout>
  );
};

export default LotteryJsonApply
