import React, { createContext, useState} from "react";

export const StoreContext = createContext();

export const StoreProvider = ({ children }) => {  
  const [url, setUrl] = useState('');
  const [agent, setAgent] = useState("CF201");
  const [platform, setPlatform] = useState("android");

  const store ={
      urlState: [url, setUrl],
      agentState: [agent, setAgent],
      platformState: [platform, setPlatform],
  };
  return (
      <StoreContext.Provider value={store}>
         {children}
      </StoreContext.Provider>
     );
};