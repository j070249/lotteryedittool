import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import FirstPage from '../containers/firstPage'
import { StoreProvider} from '../provider/storeProvider'

export default function router() {
  return (
    <StoreProvider>
      <Router>
        <div>
          <Switch>
            <Route path="/">
              <FirstPage />
            </Route>
          </Switch>
        </div>
      </Router>
    </StoreProvider>
  );
}